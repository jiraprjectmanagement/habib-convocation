
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University – Convocation – 2021</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->

<?php include 'include/header-inner.php' ?>




     <!-- inner banner -->
   <!-- <section class="inner-banner banner-21">
      <div class="conatiner">
         <div class="iner-baner--content">
            <h1>
               Convocation
               <span>2021</span>
            </h1>
         </div>
      </div>
   </section> -->
   <!-- inner banner -->

   <section class="para-area">
     <div class="container">
        <section class="sec-heading">
           <!-- <h5>Be the next generaton of</h5> -->
           <h1>#HUGRADS2021</h1>
           <!-- <p>On July 18, 2021, Habib University conducted its third convocation – online – to celebrate the Class of 2020 – the Trailblazers – and recognize their achievements.</p> -->
        </section>
     </div>
   </section>

      <div class="inner-pages-wraper">


         <!-- Inner Video -->
         <section class="main-iner-video">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="video-banner">
                        <img src="img/convo21/video-banner-2021.jpg" class="img-fluid iner-banner" alt="">
                     <img src="img/convo21/video-banner-2021-resp.jpg" class="img-fluid inner-res-banner" alt="">
                        <a class="play-icon-box" href="https://www.youtube.com/watch?v=1R1OHMfwwvw&ab_channel=HabibUniversity" data-fancybox="gallery">
                           <img src="img/playicon.svg" alt="">
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Inner Video -->
  <!-- Division Box -->   
  <section class="division-box graduate-box padt-set">
            <div class="container">
               <div class="row justify-content-between">
                  <div class="col-lg-5 col-sm-6">
                     <div class="division-img">
                        <img src="img/convo21/2.png" alt="Anwar Maqsood" class="division-mage">
                        <a data-fancybox="gallery" href="https://www.youtube.com/watch?v=8c_Rx9XPVe4&list=PLVyH-94EYRpqwQBH-JRPbXYIRpkdRpH_Z&index=14" class="play-icon-box">
                           <img src="img/playicon.svg" alt="">
                        </a>
                     </div>
                  </div>
                  <div class="col-lg-6 col-sm-6">
                     <div class="divsion-content">
                        <h3>Keynote Speaker</h3>
                        <h5>Zia Mohyeddin</h5>
                        <p class="mb-4">Zia Mohyeddin is a legend in his lifetime. A man of many talents—his career spans acting, directing, writing, broadcasting, and a wide range of aesthetic disciplines. Born in Faisalabad, he graduated from Government College, Lahore. He later studied at the Royal Academy of Dramatic Arts in London and on his return to Pakistan, he produced, directed, and acted in numerous plays.</p>
                        <p>Returning to London, he worked at the Guildford Repertory Theatre where he was picked up for the highly successful play A Passage to India. In 1970, he came to Pakistan and presented the now legendary Zia Mohyeddin Show on PTV. Later, he accepted the post of Director at the PIA Arts Academy. Returning to England again, he worked in stage plays and films but used television as his main medium, producing Here and Now, and Britain’s first Asian soap Family Pride in which he also starred. In 2005, Zia Mohyeddin set up the National Academy of Performing Arts (NAPA) in Karachi. He is also the author of three books: A Carrot is a Carrot Theatrics and The God of My Idolatry.</p>
                       
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Division Box -->
      
         

         <!-- Division Box -->
         <section class="division-box graduate-box  padtb-set">
            <div class="container">
               <div class="row justify-content-between">
                  <div class="col-lg-5 col-sm-6">
                     <div class="division-img">
                        <img src="img/convo21/1.png" alt="Imran Ismail" class="division-mage">
                        <a data-fancybox="gallery"  href="https://www.youtube.com/watch?v=s0v2IE-ck0k&list=PLVyH-94EYRpqwQBH-JRPbXYIRpkdRpH_Z&index=19" class="play-icon-box">
                           <img src="img/playicon.svg" alt="">
                        </a>
                     </div>
                  </div>
                  <div class="col-lg-6 col-sm-6">
                     <div class="divsion-content">
                        <h3>Wasif A. Rizvi</h3>
                        <h5>President</h5>
                        <p class="mb-4">Wasif Rizvi is the founding president of Habib University, Pakistan’s first undergraduate liberal arts and science institution. He is an ardent advocate of providing students a student-centric, interdisciplinary and contextual intellectual experience.</p>
                        <p class="mb-4">His achievements are visible through Habib University’s unmatched global partnerships, distinguished faculty, innovative curriculum, diverse student body and an award winning campus design.</p>
                        <p>He holds twin graduate degrees from Harvard Kennedy School and Harvard School of Education and prior to founding Habib University was associated with the Aga Khan Education Service in Pakistan and many other development projects across Asia and Africa over the last two decades.</p>
                        
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Division Box -->
      
      
       
      
         <!-- Division Box -->   
         <section class="division-box graduate-box padb-set">
            <div class="container">
               <div class="row justify-content-between">
                  <div class="col-lg-5 col-sm-6">
                     <div class="division-img">
                        <img src="img/convo21/3.png" alt="Hasan Ul Haq" class="division-mage">
                        <a data-fancybox="gallery" href="https://www.youtube.com/watch?v=u4hKu33bmwY&list=PLVyH-94EYRpqwQBH-JRPbXYIRpkdRpH_Z&index=16" class="play-icon-box">
                           <img src="img/playicon.svg" alt="">
                        </a>
                     </div>
                  </div>
                  <div class="col-lg-6 col-sm-6">
                     <div class="divsion-content">
                        <h3>Rafiq M. Habib</h3>
                        <h5>Chancellor</h5>
                        <p>Mr. Rafiq M. Habib is Chancellor of Habib University and Chairman of the Habib University Foundation. As the former head of the House of Habib, he possesses decades of business experience in insurance, banking and industry. Mr. Habib studied at St. Patrick High School, where after completing a Diploma Course in Banking, he later qualified for the Associate Membership of the Life Insurance Management Institute, New York. Subsequently, he also completed the first Advance Management Program conducted by the Harvard Business School, USA in Pakistan.</p>
                     
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Division Box -->
      
    


         <section class="graduate-area">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Graduate Directory</h3>
                     <p>The graduate directory serve as depository of information for recruitment by potential employers.</p>
                     <a target="_blank" href="https://habib.edu.pk/career-services/request-for-graduate-directory/" target="_blank" class="cooming-soon-btn">
                        <div class="btn-hover-down">
                           <span class="pdf-coming"><i class="far fa-file-pdf"></i>  Download Now</span>
                           <span class="pdf-download"><i class="fas fa-download"></i>  Download Now</span>
                        </div>
                     </a>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="grdaute-heading">
                   <img src="img/graduate-text.svg" alt="">
                  </div>
               </div>
            </div>
         </div>
      </section>
      </div>  



      

<?php include 'include/footer.php' ?>