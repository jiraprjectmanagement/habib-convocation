
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University – Convocation – 2021</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->

<?php include 'include/header-inner.php' ?>




     <!-- inner banner -->
   <!-- <section class="inner-banner banner-21">
      <div class="conatiner">
         <div class="iner-baner--content">
            <h1>
               Convocation
               <span>2021</span>
            </h1>
         </div>
      </div>
   </section> -->
   <!-- inner banner -->

   <section class="para-area">
     <div class="container">
        <section class="sec-heading">
           <!-- <h5>Be the next generaton of</h5> -->
           <h1>#HUGRADS2021</h1>
           <!-- <p>On July 18, 2021, Habib University conducted its third convocation – online – to celebrate the Class of 2020 – the Trailblazers – and recognize their achievements.</p> -->
        </section>
     </div>
   </section>

      <div class="inner-pages-wraper">


         <!-- Inner Video -->
         <section class="main-iner-video">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="video-banner">
                        <img src="img/convo21/video-banner-2021.jpg" class="img-fluid iner-banner" alt="">
                     <img src="img/convo21/video-banner-2021-resp.jpg" class="img-fluid inner-res-banner" alt="">
                        <a class="play-icon-box" href="https://www.youtube.com/watch?v=1R1OHMfwwvw&ab_channel=HabibUniversity" data-fancybox="gallery">
                           <img src="img/playicon.svg" alt="">
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Inner Video -->
  <!-- Division Box -->   
  <section class="division-box graduate-box padt-set">
            <div class="container">
               <div class="row justify-content-between">
                  <div class="col-lg-5 col-sm-6">
                     <div class="division-img">
                        <img src="img/convo21/2.png" alt="Anwar Maqsood" class="division-mage">
                        <a data-fancybox="gallery" href="https://www.youtube.com/watch?v=8c_Rx9XPVe4&list=PLVyH-94EYRpqwQBH-JRPbXYIRpkdRpH_Z&index=14" class="play-icon-box">
                           <img src="img/playicon.svg" alt="">
                        </a>
                     </div>
                  </div>
                  <div class="col-lg-6 col-sm-6">
                     <div class="divsion-content">
                        <h3>Keynote Speaker</h3>
                        <h5>Anwar Maqsood</h5>
                        <p>A renowned and highly respected scriptwriter, satirist, humorist and television presenter as the commencement speaker for the university’s second virtual and overall fourth convocation ceremony, celebrating the Pathfinders’ completion of their journey in the challenges and hardships of the global pandemic. Mr. Maqsood, popularly known for his fearlessness, caustic wit and love for telling the bitter tales addressed the Habib students in his signature style, using rich metaphors and poetic references making his speech a treat for the listeners.</p>
                       
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Division Box -->
      
         

         <!-- Division Box -->
         <section class="division-box graduate-box  padtb-set">
            <div class="container">
               <div class="row justify-content-between">
                  <div class="col-lg-5 col-sm-6">
                     <div class="division-img">
                        <img src="img/convo21/1.png" alt="Imran Ismail" class="division-mage">
                        <a data-fancybox="gallery"  href="https://www.youtube.com/watch?v=s0v2IE-ck0k&list=PLVyH-94EYRpqwQBH-JRPbXYIRpkdRpH_Z&index=19" class="play-icon-box">
                           <img src="img/playicon.svg" alt="">
                        </a>
                     </div>
                  </div>
                  <div class="col-lg-6 col-sm-6">
                     <div class="divsion-content">
                        <h3>Honorable Governor, Sindh</h3>
                        <h5>Imran Ismail</h5>
                        <p class="mb-4">Born on January 1, 1966, in Karachi, Imran Ismail is the 33rd Governor of Sindh. He completed his graduation from Government National College, Karachi, and later moved abroad to Italy and the United States, where he studied chemical and leather technology.</p>
                        <p>Mr. Ismail began taking an active part in social and welfare work in 1996 as a volunteer for Shaukat Khanum Memorial Trust. In 2018 general elections, Mr. Ismail ran from PS-111 (previously PS-112) where he was elected as a member of Sindh Assembly, which he later resigned from to take up the responsibilities of the Governor of Sindh on August 27, 2018.</p>
                        
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Division Box -->
      
      
       
      
         <!-- Division Box -->   
         <section class="division-box graduate-box padb-set">
            <div class="container">
               <div class="row justify-content-between">
                  <div class="col-lg-5 col-sm-6">
                     <div class="division-img">
                        <img src="img/convo21/3.png" alt="Hasan Ul Haq" class="division-mage">
                        <a data-fancybox="gallery" href="https://www.youtube.com/watch?v=u4hKu33bmwY&list=PLVyH-94EYRpqwQBH-JRPbXYIRpkdRpH_Z&index=16" class="play-icon-box">
                           <img src="img/playicon.svg" alt="">
                        </a>
                     </div>
                  </div>
                  <div class="col-lg-6 col-sm-6">
                     <div class="divsion-content">
                        <h3>Valedictorian</h3>
                        <h5>Hasan Ul Haq</h5>
                        <h6> Habib University’s Valedictorian for 2021</h6>
                        <p>It is an honor to be standing here today, representing our collective journey as individuals who joined Habib in August 2017, and as the fourth batch of our university, we completed it and made our young university whole.” Habib University’s Valedictorian Hassan ul Haq from the Communication & Design Program (Class of 2021), reminisces about his special journey at the varsity and also applauded Pathfinders for accomplishing this memorable milestone.</p>
                     
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Division Box -->
      
    


         <section class="graduate-area">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Graduate Directory</h3>
                     <p>The graduate directory serve as depository of information for recruitment by potential employers.</p>
                     <a target="_blank" href="https://habib.edu.pk/career-services/request-for-graduate-directory/" target="_blank" class="cooming-soon-btn">
                        <div class="btn-hover-down">
                           <span class="pdf-coming"><i class="far fa-file-pdf"></i>  Download Now</span>
                           <span class="pdf-download"><i class="fas fa-download"></i>  Download Now</span>
                        </div>
                     </a>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="grdaute-heading">
                   <img src="img/graduate-text.svg" alt="">
                  </div>
               </div>
            </div>
         </div>
      </section>
      </div>  



      

<?php include 'include/footer.php' ?>