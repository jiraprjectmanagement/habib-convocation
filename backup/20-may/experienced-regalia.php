<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University – Convocation - Regalia</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->

<?php include 'include/header-inner.php' ?>



     <!-- Inner Banner -->
     <!-- <section class="graduation-miles-banner conv-spea-banner">
      <div class="container">
         <div class="row align-content-center">
            <div class="col-lg-6">
               <div class="banner-cont-grad">
                  <h1 class="banner-title">
                     Experience the  
                   <span>Regalia</span>
                  </h1>
               </div>
            </div>
            <div class="col-lg-6">
               <img src="img/speakers/banner.jpg" class="img-fluid" alt="">
            </div>
         </div>
      </div>
   </section>-->
   <section class="graduation-miles-banner banner-24">
     <div class="container">
        <div class="row align-content-center">
           <div class="col-lg-6">
              <div class="banner-cont-grad">
                 <h1 class="banner-title">
                 Experience the    
                    <span>Regalia</span>
                 </h1>
              </div>
           </div>
        </div>
     </div>
  </section>
    <!-- Inner Banner -->

   <section class="para-area">
      <div class="container">
         <section class="sec-heading">
            <!-- <h5>Be the next generaton of</h5> -->
            <h1>#HURegalia</h1>
            <p>The Habib regalia reflects the institution’s core value of aesthetics, and the university’s pedagogical focus on contextuality and inheritance. The silhouette is slender and gender-neutral, and the palette is restrained to the official colors of white, purple and dull gold. The pattern, designed originally for the convocation, is the word Habib in Urdu, abstracted to a four-petalled flower. All gowns, except the Chancellor’s, are white, with dull gold on purple trimmings. All students and faculty wear the same gown, irrespective of the program of study, embodying the liberal arts spirit of the institution. The traditional form of the gown is supplemented by a standard white graduation cap, in a nod to the institution’s global linkages.</p>
         </section>
      </div>
    </section>

<section class="registaion-form-area regalia">
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-lg-11">
            <div class="row justify-content-center regalia-responsove-slider">
               <div class="col-lg-4">
                  <div class="rgist-box">
                        <img src="img/regalia/1.png" class="img-fluid" alt="Chancellor's">
                        <h3>Chancellor's</h3>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="rgist-box">
                        <img src="img/regalia/2.png" class="img-fluid" alt="President">
                        <h3>President</h3>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="rgist-box">
                        <img src="img/regalia/3.png" class="img-fluid" alt="Governor">
                        <h3>Governor</h3>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="rgist-box">
                        <img src="img/regalia/4.png" class="img-fluid" alt="Keynote Speaker">
                        <h3>Keynote Speaker</h3>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="rgist-box">
                        <img src="img/regalia/5.png" class="img-fluid" alt="Faculty">
                        <h3>Faculty</h3>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="rgist-box">
                        <img src="img/regalia/6.png" class="img-fluid" alt="Students">
                        <h3>Students</h3>
                  </div>
               </div>
               
            </div>
         </div>
      </div>
   </div>
</section>

  

<?php include 'include/footer.php' ?>