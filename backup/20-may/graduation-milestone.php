
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University – Convocation - Graduation Milestone</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
<?php include 'include/header-inner.php' ?>


  <section class="graduation-miles-banner banner-23">
     <div class="container">
        <div class="row align-content-center">
           <div class="col-lg-6">
              <div class="banner-cont-grad">
                 <h1 class="banner-title">
                    Graduation  
                    <span>Milestone</span>
                 </h1>
                 <p>All you need to know before graduating </p>
              </div>
           </div>
           <!-- <div class="col-lg-6">
              <img src="img/graudation-miles/banner.png" class="img-fluid" alt="">
           </div> -->
        </div>
     </div>
  </section>



  <section class="graduation-para">
     <div class="container">
        <div class="row padtb-set">
           <div class="col-lg-12">
              <div class="inner-grad-cont">
                 <h4>Graduation Milestones</h4>
               <p>The Office of Academic Systems and Registrar certifies the graduation of all candidates for all degrees. This includes collecting the students’ Intent to Graduate forms, validating the information for all academic requirements, and preparing and coordinating the distribution of degrees and final transcripts.</p>
              </div>
           </div>
        </div>
        <div class="row justify-content-between padb-set">
           <div class="col-lg-2">
              <div class="miles-count">
                 <h1>
                    01
                 </h1>
              </div>
           </div>
           <div class="col-lg-10">
              <div class="miles-content">
                 <h4>Requirements</h4>
                 <p class="mb-3">The University Convocation takes place at the end of the Spring semester every year. Students who (i) have graduated in preceding semesters of the academic year, (ii) are on-track to graduate at the end of Spring semester, and (iii) are expected to graduate in the following Summer, are eligible to participate in the Convocation for that year.</p>
                  <p>A student applying for graduation must satisfy all University requirements regardless of degree or major in which they are enrolled. In addition, they are also required to fulfill program-specific requirements identified by their respective programs.</p>
              </div>
           </div>
        </div>
        
        <div class="row justify-content-between padb-set">
         <div class="col-lg-2">
            <div class="miles-count">
               <h1>
                  02
               </h1>
            </div>
         </div>
         <div class="col-lg-10">
            <div class="miles-content">
               <h4>Circular Requirements</h4>
               <p class="mb-3"> <strong> University Liberal Core:</strong> A student must complete all requirements of the Habib Liberal Core, as described in the Academic Catalogue of the induction year.</p>
                <p class="mb-3"> <strong>Minimum Cumulative Grade Point Average (CGPA):</strong> Students must have a minimum cumulative GPA of 2.33 at the time of graduation.</p>
                <p class="mb-3"> <strong>Minimum Credit Hours Requirement:</strong></p>

                <div class="programes-area">
                   <div class="row no-gutters">
                      <div class="col-lg-6 col-6">
                         <div class="program-box prog-title">
                            <h5>Program/Major</h5>
                         </div>
                      </div>
                      <div class="col-lg-6 col-6">
                         <div class="program-box prog-title">
                            <h5>Minimum Credit Hours Requirement</h5>
                         </div>
                     </div>
                     <div class="col-lg-6 col-6 br-r br-l br-b">
                        <div class="program-box">
                           <strong>Social Development and policy</strong>
                        </div>
                     </div>
                     <div class="col-lg-6 col-6 br-r br-b">
                        <div class="program-box">
                           <strong class="major">124</strong>
                        </div>
                     </div>
                     <div class="col-lg-6 col-6 br-r br-l br-b">
                       <div class="program-box">
                          <strong>Communication and Design</strong>
                       </div>
                     </div>
                     <div class="col-lg-6 col-6 br-r br-b">
                      <div class="program-box">
                         <strong class="major">124</strong>
                      </div>
                     </div>
                     <div class="col-lg-6 col-6 br-r br-l br-b">
                      <div class="program-box">
                         <strong>Computer Science</strong>
                      </div>
                     </div>
                     <div class="col-lg-6 col-6 br-r br-b">
                      <div class="program-box">
                         <strong class="major">130</strong>
                      </div>
                     </div>
                     <div class="col-lg-6 col-6 br-r br-l br-b">
                      <div class="program-box">
                         <strong>Electrical Engineering</strong>
                      </div>
                     </div>
                     <div class="col-lg-6 col-6 br-r br-b">
                      <div class="program-box">
                         <strong class="major">134</strong>
                      </div>
                     </div>
                   </div>
                </div>

                <p class="mt-3"> <strong>Transfer of Credits:</strong> All transfer of credits must be processed in accordance with the Habib University Transfer of Credits Policy by the last day of enrolment of in the Spring semester of the fourth year.</p>
            </div>
         </div>
        </div>

        
        <div class="row justify-content-between padb-set">
         <div class="col-lg-2">
            <div class="miles-count">
               <h1>
                  03
               </h1>
            </div>
         </div>
         <div class="col-lg-10">
            <div class="miles-content">
               <h4>R2G (Road to Graduation) Program Requirements</h4>
               <p class="mb-3">Students entering their junior/senior year must fulfil R2G requirements to ensure their readiness for their transition to the job market, graduate schools, or to launch their entrepreneurial venture. R2G requirements can be met by:</p>
                <p class="mb-3">1. Attending mandatory workshops, events and activities organized by the Office of Academic Performance & Career Services.</p>
                <p>2. Developing the tools essential for their career transition.</p>
            </div>
         </div>
        </div>

        <div class="row justify-content-between padb-set">
         <div class="col-lg-2">
            <div class="miles-count">
               <h1>
                  04
               </h1>
            </div>
         </div>
         <div class="col-lg-10">
            <div class="miles-content">
               <h4>Declaration of Minor(s) Submission Deadline</h4>
               <p>Students must declare any minor(s) before submitting their ‘Intent to Graduate’. Otherwise, the minor may not appear on the final transcript.</p>
            </div>
         </div>
        </div>

        <div class="row justify-content-between padb-set">
         <div class="col-lg-2">
            <div class="miles-count">
               <h1>
                  05
               </h1>
            </div>
         </div>
         <div class="col-lg-10">
            <div class="miles-content">
               <h4>R2G (Road to Graduation) Program Requirements</h4>
               <p class="mb-3">Student must be in good academic, conduct, and financial standings:</p>
                <p class="mb-3"><strong>Good Academic Standing: </strong> All students must be in good Academic Standing as defined in the Academic Standing Policy stated in the Academic Catalog and any subsequent addendums.</p>
                <p class="mb-3"><strong>Good Conduct Standing:  </strong> All students must be in good Conduct Standing as defined in the Conduct Standing Policy.</p>
                <p><strong>Good Financial Standing:  </strong> All students must be in good Financial Standing as defined by the Financial Standing Policy</p>

            </div>
         </div>
        </div>

        <div class="row justify-content-between padb-set">
         <div class="col-lg-2">
            <div class="miles-count">
               <h1>
                  06
               </h1>
            </div>
         </div>
         <div class="col-lg-10">
            <div class="miles-content">
               <h4>Program-Specific Requirements</h4>
               <p>For specific program requirements, you may refer to the graduation requirement grid of the relevant program in the Academic Catalogue of the induction year, and any subsequent changes as approved by the competent academic bodies.</p>
            </div>
         </div>
        </div>

        <div class="row justify-content-between padb-set">
         <div class="col-lg-2">
            <div class="miles-count">
               <h1>
                  07
               </h1>
            </div>
         </div>
         <div class="col-lg-10">
            <div class="miles-content">
               <h4>Finances</h4>
               <p>Students are expected to settle all financial obligations to the University.</p>
            </div>
         </div>
        </div>

        <div class="row justify-content-between padb-set">
         <div class="col-lg-2">
            <div class="miles-count">
               <h1>
                  08
               </h1>
            </div>
         </div>
         <div class="col-lg-10">
            <div class="miles-content">
               <h4>Intent to Graduate Submission Deadline</h4>
               <p>Students will be given final approval for graduation by Office of Academic Systems and Registrar before convocation.</p>
            </div>
         </div>
        </div>

        <div class="row justify-content-between padb-set">
         <div class="col-lg-2">
            <div class="miles-count">
               <h1>
                  09
               </h1>
            </div>
         </div>
         <div class="col-lg-10">
            <div class="miles-content">
               <h4>Receive Final Approval for Graduation</h4>
               <p>Students will be given final approval for graduation by office of registrar before convocation.</p>
            </div>
         </div>
        </div>

     </div>
  </section>

   

<?php include 'include/footer.php' ?>