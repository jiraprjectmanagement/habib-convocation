<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University – Convocation – 2020</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->

<?php include 'include/header-inner.php' ?>




   <!-- inner banner -->
   <!-- <section class="inner-banner banner-20">
      <div class="conatiner">
         <div class="iner-baner--content">
            <h1>
               Convocation
               <span>2020</span>
            </h1>
         </div>
      </div>
   </section> -->
   <!-- inner banner -->


   <section class="para-area">
     <div class="container">
        <section class="sec-heading">
           <!-- <h5>Be the next generaton of</h5> -->
           <h1>#HUGRADS2020</h1>
           <p>On July 18, 2020, Habib University conducted its third convocation – online – to celebrate the Class of 2020 – the Trailblazers – and recognize their achievements.</p>
        </section>
     </div>
   </section>


   <div class="inner-pages-wraper">

       <!-- Inner Video -->
       <section class="main-iner-video">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <div class="video-banner">
                     <img src="img/convo20/video-banner-2020.jpg" class="img-fluid iner-banner" alt="">
                     <img src="img/convo20/video-banner-2020-resp.jpg" class="img-fluid inner-res-banner" alt="">
                     <a class="play-icon-box" href="https://youtu.be/Tc8m0IiESUo" data-fancybox="gallery">
                        <img src="img/playicon.svg" alt="">
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Inner Video -->

 <!-- Division Box -->   
 <section class="division-box graduate-box padt-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/convo20/2.png" alt="Dr. Azra Raza" class="division-mage">
                     <a  href="https://youtu.be/8s67KDSKZhU" data-fancybox="gallery" class="play-icon-box">
                        <img src="img/playicon.svg" alt="">
                     </a>
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Keynote Speaker</h3>
                     <h5>Dr. Azra Raza</h5>
                     <h6>PHYSICIAN, SCIENTIST, AUTHOR</h6>
                     <p>Dr. Azra Raza is the Chan Soon-Shiong Professor of Medicine and Director of the MDS Center at Columbia University in New York. Previously, she was the Chief of Hematology-Oncology and the Gladys Smith Martin Professor of Oncology at the University of Massachusetts.</p>
                   
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->

      <!-- Division Box -->
      <section class="division-box graduate-box padtb-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/convo20/1.png" alt="Imran Ismail" class="division-mage">
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Honorable Governor, Sindh</h3>
                     <h5>Imran Ismail</h5>
                     <p class="mb-4">Born on January 1, 1966, in Karachi, Imran Ismail is the 33rd Governor of Sindh. He completed his graduation from Government National College, Karachi, and later moved abroad to Italy and the United States, where he studied chemical and leather technology.</p>
                     <p>Mr. Ismail began taking an active part in social and welfare work in 1996 as a volunteer for Shaukat Khanum Memorial Trust. In 2018 general elections, Mr. Ismail ran from PS-111 (previously PS-112) where he was elected as a member of Sindh Assembly, which he later resigned from to take up the responsibilities of the Governor of Sindh on August 27, 2018.</p>
                   
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->
   
   
     
   
   
      <!-- Division Box -->   
      <section class="division-box graduate-box padb-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/convo20/3.png" alt="Shallum Oscar David" class="division-mage">
                     <a  href="https://youtu.be/P459DOpe4CA" data-fancybox="gallery" class="play-icon-box">
                        <img src="img/playicon.svg" alt="">
                     </a>
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Valedictorian</h3>
                     <h5>Shallum Oscar David</h5>
                     <h6>Valedictorian, Class of 2020</h6>
                     <p>"I am privileged to represent a class whose strength and determination in the face of great adversity is so inspirational. Our readiness to profess unity and compassion in the most challenging of times is a testament to our dedication to the practice of these values. Be it the uncertainty brought by a pandemic, or the untimely demise of a beloved Professor, together we have remained patient and resilient and finally reached this milestone."</p>
                   
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->
   
   
   
   


      
      <section class="graduate-area">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Graduate Directory</h3>
                     <p>The graduate directory serve as depository of information for recruitment by potential employers.</p>
                     <a target="_blank" href="https://habib.edu.pk/career-services/request-for-graduate-directory/" target="_blank" class="cooming-soon-btn">
                        <div class="btn-hover-down">
                           <span class="pdf-coming"><i class="far fa-file-pdf"></i>  Download Now</span>
                           <span class="pdf-download"><i class="fas fa-download"></i>  Download Now</span>
                        </div>
                     </a>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="grdaute-heading">
                   <img src="img/graduate-text.svg" alt="">
                  </div>
               </div>
            </div>
         </div>
      </section>
   </div>  



   <?php include 'include/footer.php' ?>