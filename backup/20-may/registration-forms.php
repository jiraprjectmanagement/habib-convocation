
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University &#8211; Convocation 2021 &#8211; Habib University</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
<?php include 'include/header-inner.php' ?>



  <section class="graduation-miles-banner registraion-banner">
     <div class="container">
        <div class="row align-content-center">
           <div class="col-lg-6">
              <div class="banner-cont-grad">
                 <h1 class="banner-title">
                    Registration &  
                    <span>Forms</span>
                 </h1>
              </div>
           </div>
           <div class="col-lg-6">
              <img src="img/registraion/banner.png" class="img-fluid" alt="">
           </div>
        </div>
     </div>
  </section>


<section class="registaion-form-area">
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-lg-11">
            <div class="row justify-content-center">
               <div class="col-lg-4 col-sm-6">
                  <div class="rgist-box">
                     <p>Grduate Directory Registration form</p>
                     <a href="" class="regis-view">
                        View
                     </a>
                  </div>
               </div>
               <div class="col-lg-4 col-sm-6">
                  <div class="rgist-box">
                     <p>Guest/Parents Registration form</p>
                     <a href="" class="regis-view">
                        View
                     </a>
                  </div>
               </div>
               <div class="col-lg-4 col-sm-6">
                  <div class="rgist-box">
                     <p>Alumni Registration form</p>
                     <a href="" class="regis-view">
                        View
                     </a>
                  </div>
               </div>
               <div class="col-lg-4 col-sm-6">
                  <div class="rgist-box">
                     <p>Faculty Awards Nomination form</p>
                     <a href="" class="regis-view">
                        View
                     </a>
                  </div>
               </div>
               <div class="col-lg-4 col-sm-6">
                  <div class="rgist-box">
                     <p>Yohsin Awards Nomination form</p>
                     <a href="" class="regis-view">
                        View
                     </a>
                  </div>
               </div>
               
            </div>
         </div>
      </div>
   </div>
</section>


<?php include 'include/footer.php' ?>