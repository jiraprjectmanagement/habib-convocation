<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University &#8211; Convocation 2021 &#8211; Habib University</title>
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <meta name="author" content="">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->

<?php include 'include/header-inner.php' ?>

   <!-- Inner Banner -->
  <section class="graduation-miles-banner announcement-banner">
     <div class="container">
        <div class="row align-content-center">
           <div class="col-lg-6">
              <div class="banner-cont-grad">
                 <h1 class="banner-title">
                  Convo21
                  <span>Announcements</span>
                 </h1>
              </div>
           </div>
              <div class="banner-right">
                 <img src="img/announcement/banner.png" class="img-fluid" alt="">
              </div>
        </div>
     </div>
  </section>

   <!-- Inner Banner -->


  <section class="announcement-area">
     <div class="container">
        <div class="anouncement-content">
           <h3>Announcement Title</h3>
           <p>02/03/2021</p>
           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        </div>

        <div class="post-box">
           <div class="post-item">
              <img src="img/announcement/1" alt="">
           </div>
           <div class="post-item">
              <img src="img/announcement/2" alt="">
           </div>
           
           <div class="post-item">
              <img src="img/announcement/3" alt="">
           </div>
        </div>
        <div class="anouncement-btn">
           <a href=""></a>
           <a href=""></a>
        </div>
     </div>
  </section>

   

<?php include 'include/footer.php' ?>