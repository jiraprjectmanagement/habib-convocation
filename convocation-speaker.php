
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University – Convocation - Speakers</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->

<?php include 'include/header-inner.php' ?>


   <!-- Inner Banner -->
  <!-- <section class="graduation-miles-banner conv-spea-banner">
     <div class="container">
        <div class="row align-content-center">
           <div class="col-lg-6">
              <div class="banner-cont-grad">
                 <h1 class="banner-title">
                  Convocation  
                  <span>Speakers</span>
                 </h1>
              </div>
           </div>
           <div class="col-lg-6">
              <img src="img/speakers/banner.jpg" class="img-fluid" alt="">
           </div>
        </div>
     </div>
  </section> -->

  
  <section class="graduation-miles-banner banner-27">
     <div class="container">
        <div class="row align-content-center">
           <div class="col-lg-6">
              <div class="banner-cont-grad">
                 <h1 class="banner-title">
                 Convocation   
                    <span>Speakers</span>
                 </h1>
              </div>
           </div>
        </div>
     </div>
  </section>

   <!-- Inner Banner -->


   <div class="inner-pages-wraper pt-0">
   
     <!-- Division Box -->
     <!-- <section class="division-box graduate-box padt-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/Zia-Mohyeddin-keynote-speaker.png" alt="" class="division-mage">
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Zia Mohyeddin </h3>
                     <h5>Keynote Speaker </h5>
                     <p class="mb-4">Zia Mohyeddin is a legend in his lifetime. A man of many talents—his career spans acting, directing, writing, broadcasting, and a wide range of aesthetic disciplines. Born in Faisalabad, he graduated from Government College, Lahore. He later studied at the Royal Academy of Dramatic Arts in London and on his return to Pakistan, he produced, directed, and acted in numerous plays. </p>
                     <p>Returning to London, he worked at the Guildford Repertory Theatre where he was picked up for the highly successful play A Passage to India. In 1970, he came to Pakistan and presented the now legendary Zia Mohyeddin Show on PTV. Later, he accepted the post of Director at the PIA Arts Academy. Returning to England again, he worked in stage plays and films but used television as his main medium, producing Here and Now, and Britain’s first Asian soap Family Pride in which he also starred. In 2005, Zia Mohyeddin set up the National Academy of Performing Arts (NAPA) in Karachi. He is also the author of three books: A Carrot is a Carrot Theatrics and The God of My Idolatry.</p>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
      <!-- Division Box -->
      
       <!-- Division Box -->
       <section class="division-box graduate-box padtb-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/speakers/4.png" alt="" class="division-mage" alt="Wasif A. Rizvi">
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Wasif A. Rizvi</h3>
                     <h5>President</h5>
                     <p class="mb-4">Wasif Rizvi is the founding president of Habib University, Pakistan’s first undergraduate liberal arts and science institution. He is an ardent advocate of providing students a student-centric, interdisciplinary and contextual intellectual experience.</p>
                     <p class="mb-4">His achievements are visible through Habib University’s unmatched global partnerships, distinguished faculty, innovative curriculum, diverse student body and an award winning campus design.</p>
                     <p>He holds twin graduate degrees from Harvard Kennedy School and Harvard School of Education and prior to founding Habib University was associated with the Aga Khan Education Service in Pakistan and many other development projects across Asia and Africa over the last two decades.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->


      <!-- Division Box -->
      <section class="division-box graduate-box padb-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/speakers/3.png" alt="" class="division-mage" alt="Rafiq M. Habib">
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Rafiq M. Habib</h3>
                     <h5>Chancellor</h5>
                     <p>Mr. Rafiq M. Habib is Chancellor of Habib University and Chairman of the Habib University Foundation. As the former head of the House of Habib, he possesses decades of business experience in insurance, banking and industry. Mr. Habib studied at St. Patrick High School, where after completing a Diploma Course in Banking, he later qualified for the Associate Membership of the Life Insurance Management Institute, New York. Subsequently, he also completed the first Advance Management Program conducted by the Harvard Business School, USA in Pakistan.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->
   



   </div>

   

<?php include 'include/footer.php' ?>