








// Set the date we're counting down to
var countDownDate = new Date("June 4, 2022 18:00:00").getTime();

// Update the count down every 1 second
var countdownfunction = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now an the count down date
  var distance = countDownDate - now;
 
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  
  if (days < 10 ) {
    days = '0' + days;
  }
  if (hours < 10 ) {
    hours = '0' + hours;
  }
  if (minutes < 10 ) {
    minutes = '0' + minutes;
  }
  if (seconds < 10 ) {
    seconds = '0' + seconds;
  }

  // Output the result in an element with id="custom-timer"
  document.getElementById("custom-timer").innerHTML = 
  "<div class='timer-box'>" +
    "<span class='timer-time'>" + days + "</span>" + "<span class='timer-text'>Days</span> " +
  "</div>" +
  "<div class='timer-box'>" +
     "<span class='timer-time'>" + hours + "</span>" + "<span class='timer-text'>Hours</span> " +
  "</div>" +
  "<div class='timer-box'>" +
      "<span class='timer-time'>" + minutes + "</span>" + "<span class='timer-text'>Minutes</span> " +
  "</div>" +
  "<div class='timer-box'>" +
    "<span class='timer-time'>" + seconds+ "</span>" + "<span class='timer-text'>seconds</span> "
  "</div>";
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(countdownfunction);
    document.getElementById("custom-timer").innerHTML = "EXPIRED";
  }
}, 1000);











const Confettiful = function (el) {
    this.el = el;
    this.containerEl = null;
  
    this.confettiFrequency = 3;
    this.confettiColors = ['#ebb97e', '#c89347', '#be8c4f', '#ebb97e', '#c89347'];
    this.confettiAnimations = ['slow', 'slow', 'slow'];
  
    this._setupElements();
    this._renderConfetti();
  };
  
  Confettiful.prototype._setupElements = function () {
    const containerEl = document.createElement('div');
    const elPosition = this.el.style.position;
  
    if (elPosition !== 'relative' || elPosition !== 'absolute') {
      this.el.style.position = 'relative';
    }
  
    containerEl.classList.add('confetti-container');
  
    this.el.appendChild(containerEl);
  
    this.containerEl = containerEl;
  };
  
  Confettiful.prototype._renderConfetti = function () {
    this.confettiInterval = setInterval(() => {
      const confettiEl = document.createElement('div');
      const confettiSize = Math.floor(Math.random() * 5) + 7 + 'px';
      const confettiBackground = this.confettiColors[Math.floor(Math.random() * this.confettiColors.length)];
      const confettiLeft = Math.floor(Math.random() * this.el.offsetWidth) + 'px';
      const confettiAnimation = this.confettiAnimations[Math.floor(Math.random() * this.confettiAnimations.length)];
  
      confettiEl.classList.add('confetti', 'confetti--animation-' + confettiAnimation);
      confettiEl.style.left = confettiLeft;
      confettiEl.style.width = confettiSize;
      confettiEl.style.height = confettiSize;
      confettiEl.style.backgroundColor = confettiBackground;
  
      confettiEl.removeTimeout = setTimeout(function () {
        confettiEl.parentNode.removeChild(confettiEl);
      }, 3000);
  
      this.containerEl.appendChild(confettiEl);
    }, 25);
  };
  
  window.confettiful = new Confettiful(document.querySelector('.js-container'));
