<footer>
      <div class="container">
         <div class="footer-inner">
             <p class="fol-para">Follow Habib University</p>
             <ul>
               <li><a target="_blank" href="https://www.facebook.com/HabibUniversity"> <img src="img/fb.png" alt=""> </a></li>
               <li><a target="_blank" href="https://www.linkedin.com/company/habib-university"><img src="img/linkedin.png" alt=""></a></li>
               <li><a target="_blank" href="https://instagram.com/habibuniversity"><img src="img/instagram.png" alt=""></a></li>
               <li><a target="_blank" href="https://twitter.com/habibuniversity"><img src="img/twitter.png" alt=""></a></li>
               <li><a target="_blank" href="https://www.youtube.com/user/HabibUni"><img src="img/youtube.png" alt=""></a></li>
            </ul>
             <p class="copy-right">© Habib University. All rights reserved.</p>
         </div>
      </div>
   </footer>
      <script src="js/plugin.js?v=2.1.4"></script>
      <script src="js/custom.js?v=2.1.4"></script>

      
<!-- Modal Pre Shame -->
<div class="dsse-final-showcase">
   <div class="modal" id="DsseFinalPreSh">
      <div class="modal-dialog">
         <div class="modal-content">
               <button type="button" class="close" data-dismiss="modal">&times;</button>

                  <div class="row">
                     <div class="col-md-12">
                        <div id="custCarouselFour" class="carousel slide" data-ride="carousel" align="center">
                        <!-- slides -->
                        <div class="carousel-inner">
                           <div class="carousel-item active">
                              <img src="img/event-slider/dsse/preshame1.jpg" alt="">
                              <div class="caption-content">
                                 <p>The Class of 2022 gathered together in preparation for their upcoming farewell. </p>
                              </div>
                           </div>

                           <div class="carousel-item">
                              <img src="img/event-slider/dsse/preshame2.jpg" alt="">
                              <div class="caption-content">
                                 <!-- <p>Exploring the interdisciplinarity between Computer Science and Electrical Engineering, AMAL – An Autonomous Delivery Robot, remained one of the highlights of the exhibition. AMAL delivers food using a robot to people who might have difficulty moving.</p> -->
                              </div>
                           </div>

                           <div class="carousel-item">
                              <img src="img/event-slider/dsse/preshame3.jpg" alt="">
                              <div class="caption-content">
                                 <!-- <p>Muhammad Munawwar Anwar, a Computer Science major from the Class of 2022, presents his project to the evaluators. The project, Compression-based Perciever, was proposed as a novel architecture for computer vision problems, building up from Google DeepMind’s Perceiver.</p> -->
                              </div>
                           </div>

                           <div class="carousel-item">
                              <img src="img/event-slider/dsse/preshame4.jpg" alt="">
                              <div class="caption-content">
                                 <!-- <p>At Habib University, students are encouraged to think out of the box and develop smart solutions to real-life problems. A Novel Approach to Protein-Protein Interaction (PPI) Network Analysis was one such project that explores viral receptor binding and aids drug development among other things.</p> -->
                              </div>
                           </div>

                           <div class="carousel-item">
                              <img src="img/event-slider/dsse/preshame5.jpg" alt="">
                              <div class="caption-content">
                                 <!-- <p>Lack of accessibility remains one of the most neglected issues in Pakistan. The project, Artificial Arm Control Using EEG Based Brain Signal, aims to develop a non-invasive way for people with disabilities to control an artificial hand through reading electroencephalogram (EEG) brain signals and interpreting them as various hand movement controls that can be displayed in a computer simulation.</p> -->
                              </div>
                           </div>
                           <div class="carousel-item">
                              <img src="img/event-slider/dsse/preshame6.jpg" alt="">
                              <div class="caption-content">
                                 <!-- <p>Saathi remained one of the highlights of the day. An Urdu Virtual Assistant for Elderly people in Pakistan that facilitates them in their daily tasks, by acting as their companion, Saathi has been shortlisted for the global top 50 at 𝐆𝐨𝐨𝐠𝐥𝐞 𝐒𝐨𝐥𝐮𝐭𝐢𝐨𝐧 𝐂𝐡𝐚𝐥𝐥𝐞𝐧𝐠𝐞. The project was one of the only 2 projects to get selected from Pakistan, from over 800 submissions!</p> -->
                              </div>
                           </div>
                         

                        
                        </div>

                        <!-- Left right -->
                        <div class="carousel-item-click">
                           <a class="carousel-control-prev" href="#custCarouselFour" data-slide="prev">
                              <span class="carousel-control-prev-icon"></span>
                           </a>
                           <a class="carousel-control-next" href="#custCarouselFour" data-slide="next">
                              <span class="carousel-control-next-icon"></span>
                           </a>
                        </div>

                        <!-- Thumbnails -->
                        <ol class="carousel-indicators list-inline">
                           <li class="list-inline-item active">
                              <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#custCarouselFour">
                              <img src="img/event-slider/dsse/preshame1.jpg" class="img-fluid">
                              </a>
                           </li>

                           <li class="list-inline-item">
                              <a id="carousel-selector-1" data-slide-to="1" data-target="#custCarouselFour">
                              <img src="img/event-slider/dsse/preshame2.jpg" class="img-fluid">
                              </a>
                           </li>

                           <li class="list-inline-item">
                              <a id="carousel-selector-2" data-slide-to="2" data-target="#custCarouselFour">
                              <img src="img/event-slider/dsse/preshame3.jpg"  class="img-fluid">
                              </a>
                           </li>

                           <li class="list-inline-item">
                              <a id="carousel-selector-3" data-slide-to="3" data-target="#custCarouselFour">
                              <img src="img/event-slider/dsse/preshame4.jpg"  class="img-fluid">
                              </a>
                           </li>
                           
                           <li class="list-inline-item">
                              <a id="carousel-selector-4" data-slide-to="4" data-target="#custCarouselFour">
                              <img src="img/event-slider/dsse/preshame5.jpg"  class="img-fluid">
                              </a>
                           </li>
                           <li class="list-inline-item">
                              <a id="carousel-selector-5" data-slide-to="5" data-target="#custCarouselFour">
                              <img src="img/event-slider/dsse/preshame6.jpg"  class="img-fluid">
                              </a>
                           </li>
                        
                     
                           
                           </ol>
                        </div>
                     </div>
                  </div>

           

         </div>
      </div>
   </div>
</div>


       
  <!-- The Modal CND -->
  <div class="dsse-final-showcase">
         <div class="modal" id="DsseFinalCND">
            <div class="modal-dialog">
               <div class="modal-content">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <div class="row">
                           <div class="col-md-12">
                              <div id="custCarouselThree" class="carousel slide" data-ride="carousel" align="center">
                              <!-- slides -->
                              <div class="carousel-inner">
                                 <div class="carousel-item active">
                                    <img src="img/event-slider/dsse/cnd1.jpg" alt="">
                                    <div class="caption-content">
                                       <p>Muhammad Hammad Aamir Lone (CND, Class of 2022) stands with his comic artefact called the Merits and Potentials of Comics – a comic about comics that uses its narrative to show their techniques and potential.</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cnd2.jpg" alt="">
                                    <div class="caption-content">
                                       <p>Nismah Abbasi (CND, Class of 2022) sits at her board game project, Pehchaan, that involves sound to have more control over memorialization.</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cnd3.jpg" alt="">
                                    <div class="caption-content">
                                       <p>Mariyam Aslam Bawany and Fatima Nooraen (CND, Class of 2022) with their board game and book project, Kaam-Yabi, that Vocational Training Centres can use to develop, improve and instill entrepreneurship and financial literacy skills in women studying in those vocational training centers.</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cnd4.jpg" alt="">
                                    <div class="caption-content">
                                       <p>Sara Ahmad and Muniza Kazi’s exhibit, Kolachi Say Karachi tuk, was a visual display that solves an intellectual problem - the lack of knowledge of Karachi’s culture, heritage and history.</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cnd5.jpg" alt="">
                                    <div class="caption-content">
                                       <p>From L-R: Babar Shaikh (multimedia artist, filmmaker, educator from the Department of Communication Design at Indus Valley School of Art and Architecture) Syed Ali Hussain (Visiting Assistant Professor, Communication and Design at Habib University), Gul Zaib Shakeel (General Manager at Teeli, A Dawn Media Group Company) Haya Fatima Iqbal (Assistant Professor of Practice, Communication and Design at Habib University) Behzad Khosravi Noori (Assistant Professor of Practice, Comm… See more</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cnd6.jpg" alt="">
                                    <div class="caption-content">
                                       <p>Kinza Irfan’s fashion show project, Pakistan’s Urban Legends: Fashion Re-Enactment, illustrated clothes depicting local urban legends.</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cnd7.jpg" alt="">
                                    <div class="caption-content">
                                       <p>Zinnia Amin's Log Kia Kahein Gay featured a projector screen that could be seen through two different colored glasses: blue (showing a crowd through the eyes of a normal person) and red (showing a crowd through the eyes of a person with social anxiety) .</p>
                                    </div>
                                 </div>
                                 
                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cnd8.jpg" alt="">
                                    <div class="caption-content">
                                       <p>Mahnoor Nadeem’s short film Qurbat Ki Lehrein talked about language as a powerful tool in constructing realities and how it constructs and transforms the reality of a close interpersonal relationship.</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cnd9.jpg" alt="">
                                    <div class="caption-content">
                                       <p>From L-R: Ms Aqsa Junejo (Assistant Director and Head of Marketing and Communications at Habib University) attends the film screening, Khalid Malik (Actor, Radio Presenter and Entertainer), Gul Zaib Shakeel (Former General Manager at Teeli, A Dawn Media Group Company) pose for a photo before the screenings begin.</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cnd10.jpg" alt="">
                                    <div class="caption-content">
                                       <p>Students gather at the showcase to see Class of 2022’s innovative final year projects at the Playground – Habib University’s Center of Transdisciplinarity, Design and Innovation</p>
                                    </div>
                                 </div>

                              
                              </div>

                              <!-- Left right -->
                              <div class="carousel-item-click">
                                 <a class="carousel-control-prev" href="#custCarouselThree" data-slide="prev">
                                    <span class="carousel-control-prev-icon"></span>
                                 </a>
                                 <a class="carousel-control-next" href="#custCarouselThree" data-slide="next">
                                    <span class="carousel-control-next-icon"></span>
                                 </a>
                              </div>

                              <!-- Thumbnails -->
                              <ol class="carousel-indicators list-inline">
                                 <li class="list-inline-item active">
                                    <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#custCarouselThree">
                                    <img src="img/event-slider/dsse/cnd1.jpg" class="img-fluid">
                                    </a>
                                 </li>

                                 <li class="list-inline-item">
                                    <a id="carousel-selector-1" data-slide-to="1" data-target="#custCarouselThree">
                                    <img src="img/event-slider/dsse/cnd2.jpg" class="img-fluid">
                                    </a>
                                 </li>

                                 <li class="list-inline-item">
                                    <a id="carousel-selector-2" data-slide-to="2" data-target="#custCarouselThree">
                                    <img src="img/event-slider/dsse/cnd3.jpg"  class="img-fluid">
                                    </a>
                                 </li>

                                 <li class="list-inline-item">
                                    <a id="carousel-selector-3" data-slide-to="3" data-target="#custCarouselThree">
                                    <img src="img/event-slider/dsse/cnd4.jpg"  class="img-fluid">
                                    </a>
                                 </li>
                                 
                                 <li class="list-inline-item">
                                    <a id="carousel-selector-4" data-slide-to="4" data-target="#custCarouselThree">
                                    <img src="img/event-slider/dsse/cnd5.jpg"  class="img-fluid">
                                    </a>
                                 </li>
                              
                                 
                                 <li class="list-inline-item">
                                    <a id="carousel-selector-5" data-slide-to="5" data-target="#custCarouselThree">
                                    <img src="img/event-slider/dsse/cnd6.jpg"  class="img-fluid">
                                    </a>
                                 </li>
                                 
                                 <li class="list-inline-item">
                                    <a id="carousel-selector-6" data-slide-to="6" data-target="#custCarouselThree">
                                    <img src="img/event-slider/dsse/cnd7.jpg"  class="img-fluid">
                                    </a>
                                 </li>
                                 
                                 <li class="list-inline-item">
                                    <a id="carousel-selector-7" data-slide-to="7" data-target="#custCarouselThree">
                                    <img src="img/event-slider/dsse/cnd8.jpg"  class="img-fluid">
                                    </a>
                                 </li>

                                 <li class="list-inline-item">
                                    <a id="carousel-selector-8" data-slide-to="8" data-target="#custCarouselThree">
                                    <img src="img/event-slider/dsse/cnd9.jpg"  class="img-fluid">
                                    </a>
                                 </li>
                                 
                                 <li class="list-inline-item">
                                    <a id="carousel-selector-9" data-slide-to="9" data-target="#custCarouselThree">
                                    <img src="img/event-slider/dsse/cnd10.jpg"  class="img-fluid">
                                    </a>
                                 </li>

                                 
                              
                                 </ol>
                              </div>
                           </div>
                        </div>

                 

               </div>
            </div>
         </div>
      </div>




      <!-- The Modal -->
      <div class="dsse-final-showcase">
         <div class="modal" id="DsseFinalEE">
            <div class="modal-dialog">
               <div class="modal-content">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <div class="row">
                           <div class="col-md-12">
                              <div id="custCarousel" class="carousel slide" data-ride="carousel" align="center">
                              <!-- slides -->
                              <div class="carousel-inner">
                                 <div class="carousel-item active">
                                    <img src="img/event-slider/dsse/ee1.png" alt="">
                                    <div class="caption-content">
                                       <p>Electric Vehicles was one of the common themes observed in the exhibition that day as another team of Electrical Engineering showcased a project that focused on the same technology.</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/ee2.png" alt="">
                                    <div class="caption-content">
                                       <p>Muhammad Alamgir, Senior Electrical Engineer, HUBCO HPSL LTD, evaluating the project, Sustainable Vegetation (Paidaar Nabataat), that designs and monitors an aeroponics chamber for the growth of different types of fruits and vegetables indoors without the use of excessive land.</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/ee3.png" alt="">
                                    <div class="caption-content">
                                       <p>Exploring the interdisciplinarity between Computer Science and Electrical Engineering, AMAL – An Autonomous Delivery Robot, remained one of the highlights of the exhibition. AMAL delivers food using a robot to people who might have difficulty moving.</p>
                                    </div>
                                 </div>

                                
                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/ee4.jpg" alt="">
                                    <div class="caption-content">
                                       <p>Faculty from Habib University’s Dhanani School of Science and Engineering, (from left to right) Dr. Muhammad Mobeen Movania, Assistant Professor, Computer Science, Dr. Saleha Raza, Assistant Professor, Computer Science, Dr. Waqar Saleem, Computer Science, and Dr. Abdul Samad, Program Director and Assistant Professor, Computer Science, posing for a quick picture as they prepare to welcome evaluators from different industries.</p>
                                    </div>
                                 </div>
                              </div>

                              <!-- Left right -->
                              <div class="carousel-item-click">
                                 <a class="carousel-control-prev" href="#custCarousel" data-slide="prev">
                                    <span class="carousel-control-prev-icon"></span>
                                 </a>
                                 <a class="carousel-control-next" href="#custCarousel" data-slide="next">
                                    <span class="carousel-control-next-icon"></span>
                                 </a>
                              </div>

                              <!-- Thumbnails -->
                              <ol class="carousel-indicators list-inline">
                                 <li class="list-inline-item active">
                                    <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#custCarousel">
                                    <img src="img/event-slider/dsse/ee1.png" class="img-fluid">
                                    </a>
                                 </li>

                                 <li class="list-inline-item">
                                    <a id="carousel-selector-1" data-slide-to="1" data-target="#custCarousel">
                                    <img src="img/event-slider/dsse/ee2.png" class="img-fluid">
                                    </a>
                                 </li>

                                 <li class="list-inline-item">
                                    <a id="carousel-selector-2" data-slide-to="2" data-target="#custCarousel">
                                    <img src="img/event-slider/dsse/ee3.png"  class="img-fluid">
                                    </a>
                                 </li>
                                 
                                 <li class="list-inline-item">
                                    <a id="carousel-selector-3" data-slide-to="3" data-target="#custCarousel">
                                    <img src="img/event-slider/dsse/ee4.jpg"  class="img-fluid">
                                    </a>
                                 </li>
                                 </ol>
                              </div>
                           </div>
                        </div>

                 

               </div>
            </div>
         </div>
      </div>
   



         <!-- The Modal -->
      <div class="dsse-final-showcase">
         <div class="modal" id="DsseFinalCS">
            <div class="modal-dialog">
               <div class="modal-content">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <div class="row">
                           <div class="col-md-12">
                              <div id="custCarouselone" class="carousel slide" data-ride="carousel" align="center">
                              <!-- slides -->
                              <div class="carousel-inner">
                                 <div class="carousel-item active">
                                    <img src="img/event-slider/dsse/cs1.png" alt="">
                                    <div class="caption-content">
                                       <p>Habib University alumna, Yusra Afzal of Social Development and Policy, Class of 2020 learns about AugmentEd, a project by Computer Science students. This project is an Augmented Reality (AR) Edtech ecosystem of apps that enable teachers and students to produce and consume AR-first educational content with zero taps!</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cs2.png" alt="">
                                    <div class="caption-content">
                                       <p>Exploring the interdisciplinarity between Computer Science and Electrical Engineering, AMAL – An Autonomous Delivery Robot, remained one of the highlights of the exhibition. AMAL delivers food using a robot to people who might have difficulty moving.</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cs3.png" alt="">
                                    <div class="caption-content">
                                       <p>Muhammad Munawwar Anwar, a Computer Science major from the Class of 2022, presents his project to the evaluators. The project, Compression-based Perciever, was proposed as a novel architecture for computer vision problems, building up from Google DeepMind’s Perceiver.</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cs4.png" alt="">
                                    <div class="caption-content">
                                       <p>At Habib University, students are encouraged to think out of the box and develop smart solutions to real-life problems. A Novel Approach to Protein-Protein Interaction (PPI) Network Analysis was one such project that explores viral receptor binding and aids drug development among other things.</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cs5.png" alt="">
                                    <div class="caption-content">
                                       <p>Lack of accessibility remains one of the most neglected issues in Pakistan. The project, Artificial Arm Control Using EEG Based Brain Signal, aims to develop a non-invasive way for people with disabilities to control an artificial hand through reading electroencephalogram (EEG) brain signals and interpreting them as various hand movement controls that can be displayed in a computer simulation.</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cs9.jpg" alt="">
                                    <div class="caption-content">
                                       <p>Faculty from Habib University’s Dhanani School of Science and Engineering, (from left to right) Dr. Muhammad Mobeen Movania, Assistant Professor, Computer Science, Dr. Saleha Raza, Assistant Professor, Computer Science, Dr. Waqar Saleem, Computer Science, and Dr. Abdul Samad, Program Director and Assistant Professor, Computer Science, posing for a quick picture as they prepare to welcome evaluators from different industries.</p>
                                    </div>
                                 </div>

                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cs6.png" alt="">
                                    <div class="caption-content">
                                       <p>Saathi remained one of the highlights of the day. An Urdu Virtual Assistant for Elderly people in Pakistan that facilitates them in their daily tasks, by acting as their companion, Saathi has been shortlisted for the global top 50 at 𝐆𝐨𝐨𝐠𝐥𝐞 𝐒𝐨𝐥𝐮𝐭𝐢𝐨𝐧 𝐂𝐡𝐚𝐥𝐥𝐞𝐧𝐠𝐞. The project was one of the only 2 projects to get selected from Pakistan, from over 800 submissions!</p>
                                    </div>
                                 </div>
                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cs7.png" alt="">
                                    <div class="caption-content">
                                       <p>Saathi remained one of the highlights of the day. An Urdu Virtual Assistant for Elderly people in Pakistan that facilitates them in their daily tasks, by acting as their companion, Saathi has been shortlisted for the global top 50 at 𝐆𝐨𝐨𝐠𝐥𝐞 𝐒𝐨𝐥𝐮𝐭𝐢𝐨𝐧 𝐂𝐡𝐚𝐥𝐥𝐞𝐧𝐠𝐞. The project was one of the only 2 projects to get selected from Pakistan, from over 800 submissions!</p>
                                    </div>
                                 </div>
                                 <div class="carousel-item">
                                    <img src="img/event-slider/dsse/cs8.png" alt="">
                                    <div class="caption-content">
                                       <p>Seasoned professionals from the industry gather to learn more about a Computer Science project, Tezz which is a website and mobile application-based model that digitizes the ambulance system in Karachi.</p>
                                    </div>
                                 </div> 
                              

                              
                              </div>

                              <!-- Left right -->
                              <div class="carousel-item-click">
                                 <a class="carousel-control-prev" href="#custCarouselone" data-slide="prev">
                                    <span class="carousel-control-prev-icon"></span>
                                 </a>
                                 <a class="carousel-control-next" href="#custCarouselone" data-slide="next">
                                    <span class="carousel-control-next-icon"></span>
                                 </a>
                              </div>

                              <!-- Thumbnails -->
                              <ol class="carousel-indicators list-inline">
                                 <li class="list-inline-item active">
                                    <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#custCarouselone">
                                    <img src="img/event-slider/dsse/cs1.png" class="img-fluid">
                                    </a>
                                 </li>

                                 <li class="list-inline-item">
                                    <a id="carousel-selector-1" data-slide-to="1" data-target="#custCarouselone">
                                    <img src="img/event-slider/dsse/cs2.png" class="img-fluid">
                                    </a>
                                 </li>

                                 <li class="list-inline-item">
                                    <a id="carousel-selector-2" data-slide-to="2" data-target="#custCarouselone">
                                    <img src="img/event-slider/dsse/cs3.png"  class="img-fluid">
                                    </a>
                                 </li>

                                 <li class="list-inline-item">
                                    <a id="carousel-selector-3" data-slide-to="3" data-target="#custCarouselone">
                                    <img src="img/event-slider/dsse/cs4.png"  class="img-fluid">
                                    </a>
                                 </li>
                                 
                                 <li class="list-inline-item">
                                    <a id="carousel-selector-4" data-slide-to="4" data-target="#custCarouselone">
                                    <img src="img/event-slider/dsse/cs5.png"  class="img-fluid">
                                    </a>
                                 </li>
                                 <li class="list-inline-item">
                                    <a id="carousel-selector-5" data-slide-to="5" data-target="#custCarouselone">
                                    <img src="img/event-slider/dsse/cs9.jpg"  class="img-fluid">
                                    </a>
                                 </li>
                                 <li class="list-inline-item">
                                    <a id="carousel-selector-6" data-slide-to="6" data-target="#custCarouselone">
                                    <img src="img/event-slider/dsse/cs6.png"  class="img-fluid">
                                    </a>
                                 </li>
                                 <li class="list-inline-item">
                                    <a id="carousel-selector-7" data-slide-to="7" data-target="#custCarouselone">
                                    <img src="img/event-slider/dsse/cs7.png"  class="img-fluid">
                                    </a>
                                 </li>
                                 <li class="list-inline-item">
                                    <a id="carousel-selector-8" data-slide-to="8" data-target="#custCarouselone">
                                    <img src="img/event-slider/dsse/cs8.png"  class="img-fluid">
                                    </a>
                                 </li> 
                     
                           
                                 
                                 </ol>
                              </div>
                           </div>
                        </div>

                 

               </div>
            </div>
         </div>
      </div>
   


      




   </body>
</html>