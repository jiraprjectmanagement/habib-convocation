
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University – Convocation - Graduation Milestone</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
<?php include 'include/header-inner.php' ?>


  <section class="graduation-miles-banner banner-29">
     <div class="container">
        <div class="row align-content-center">
           <div class="col-lg-8">
              <div class="banner-cont-grad">
                 <h1 class="banner-title">
                    Graduation  
                    <span>Ceremony & Events</span>
                 </h1>
              </div>
           </div>
           <!-- <div class="col-lg-6">
              <img src="img/graudation-miles/banner.png" class="img-fluid" alt="">
           </div> -->
        </div>
     </div>
  </section>



  <section class="graduation-para event-area ">
     <div class="container brder-btm">
        <div class="row padtb-set">
           <div class="col-lg-12">
              <div class="inner-grad-cont">
                 <h4>Graduation Events</h4>
               <ul class="gradua-event-list">
                  <li> 
                     <span>Netflix Party</span> 
                     <span class="time-ceremony"> 5:00 pm</span> 
                  </li>
                  <li> 
                     <span>Digital Scavenger Hunt</span> 
                     <span class="time-ceremony"> 5:00 pm</span> 
                  </li>
                  <li> 
                     <span>Faculty Gratitude Day</span> 
                     <span class="time-ceremony"> 5:00 pm</span> 
                  </li>
                  <li> 
                     <span>Self Portraits – Memoir Launch</span> 
                     <span class="time-ceremony"> 5:00 pm</span> 
                  </li>
                  <li> 
                     <span>Musical Concert</span> 
                     <span class="time-ceremony"> 5:00 pm</span> 
                  </li>
                  <li> 
                     <span>Comedy Night</span> 
                     <span class="time-ceremony"> 5:00 pm</span> 
                  </li>
                  <li> 
                     <span>Alvida – A Virtual Farewell</span> 
                     <span class="time-ceremony"> 5:00 pm</span> 
                  </li>
               </ul>
              </div>
           </div>
        </div>

     </div>
  </section>
  <section class="graduation-para event-area">
     <div class="container">
        <div class="row padtb-set">
           <div class="col-lg-12">
              <div class="inner-grad-cont">
                 <h4>Graduation Ceremony </h4>
               <ul class="gradua-event-list">
                  <li> 
                     <span>Welcome & Tilawat</span> 
                     <span class="time-ceremony">6:00pm</span> 
                  </li>
                  <li> 
                     <span>National Anthem</span> 
                     <span class="time-ceremony">6:00pm</span> 
                  </li>
                  <li> 
                     <span>Declaration of Convocation Open</span> 
                     <span class="time-ceremony">6:00pm</span> 
                  </li>
                  <li> 
                     <span>Tribute to the Trailblazers – Video</span> 
                     <span class="time-ceremony">6:00pm</span> 
                  </li>
                  <li> 
                     <span>Valedictorian Speech</span> 
                     <span class="time-ceremony">6:00pm</span> 
                  </li>
                  <li> 
                     <span>Welcome Speech by President, Mr. Wasif A. Rizvi</span> 
                     <span class="time-ceremony">6:00pm</span> 
                  </li>
               </ul>
              </div>
           </div>
        </div>

     </div>
  </section>
   
  <section class="graduation-para event-area">
     <div class="container">
        <div class="row padb-set">
           <div class="col-lg-12">
              <div class="inner-grad-cont">
                 <h4>Conferring of Degrees </h4>
               <ul class="gradua-event-list">
                  <li> 
                     <span>Dhanani School of Science and Engineering</span> 
                     <span class="time-ceremony">6:15pm</span> 
                  </li>
                  <li> 
                     <span>School of Arts, Humanities and Social Sciences</span> 
                     <span class="time-ceremony">6:15pm</span> 
                  </li>
                  <li> 
                     <span>Speech by Governor Sindh, Mr. Imran Ismail</span> 
                     <span class="time-ceremony">6:15pm</span> 
                  </li>
                  <li> 
                     <span>Dean’s Awards</span> 
                     <span class="time-ceremony">6:15pm</span> 
                  </li>
                  <li> 
                     <span>School Awards</span> 
                     <span class="time-ceremony">6:15pm</span> 
                  </li>
                  <li> 
                     <span>Student Life Awards</span> 
                     <span class="time-ceremony">6:15pm</span> 
                  </li>
                  <li> 
                     <span>Commencement Speech, Dr. Azra Raza</span> 
                     <span class="time-ceremony">6:15pm</span> 
                  </li>
                  <li> 
                     <span>Speech by Chancellor, Mr. Rafiq Muhammad Habib</span> 
                     <span class="time-ceremony">6:15pm</span> 
                  </li>
                  <li> 
                     <span>Chancellor’s Yohsin Award</span> 
                     <span class="time-ceremony">6:15pm</span> 
                  </li>
                  <li> 
                     <span>Declaration of Convocation Closed</span> 
                     <span class="time-ceremony">6:15pm</span> 
                  </li>
               </ul>
              </div>
           </div>
        </div>

     </div>
  </section>

<?php include 'include/footer.php' ?>