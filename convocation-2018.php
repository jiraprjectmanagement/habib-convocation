<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University – Convocation – 2018</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      


<?php include 'include/header-inner.php' ?>

   <!-- inner banner -->
   <!-- <section class="inner-banner banner-18">
      <div class="conatiner">
         <div class="iner-baner--content">
            <h1>
               Convocation
               <span>2018</span>
            </h1>
         </div>
      </div>
   </section> -->
   <!-- inner banner -->


   <section class="para-area">
     <div class="container">
        <section class="sec-heading">
           <!-- <h5>Be the next generaton of</h5> -->
           <h1>#HUGRADS2018</h1>
           <p>On May 5, 2018, the Habib University community gathered for its first convocation and <br> celebrated the achievements of the Co-founders.</p>
        </section>
     </div>
   </section>


   <div class="inner-pages-wraper">

       <!-- Inner Video -->
       <section class="main-iner-video">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <div class="video-banner">
                     <img src="img/convo18/video-banner-2018.jpg" class="img-fluid iner-banner" alt="">
                     <img src="img/convo18/video-banner-2018-resp.jpg" class="img-fluid inner-res-banner" alt="">
                     <a class="play-icon-box" href="https://youtu.be/qSyor2AJsE0" data-fancybox="gallery">
                        <img src="img/playicon.svg" alt="">
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Inner Video -->

     
   
   
      <!-- Division Box -->   
      <section class="division-box graduate-box padt-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/convo18/2.png" alt="Syed Baber Ali" class="division-mage">
                     <a  href="https://youtu.be/5bcgRYAtYbs" data-fancybox="gallery"  class="play-icon-box">
                        <img src="img/playicon.svg" alt="">
                     </a>
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Keynote Speaker</h3>
                     <h5>Syed Baber Ali</h5>
                     <h6>Pro-Chancellor, LUMS</h6>
                     <p>Syed Babar Ali, OBE, SI, is a respected entrepreneur, industrialist and bureaucrat, who set up Pakistan’s largest food processing company, Nestlé Pakistan Limited among being influential in the success of a number of other businesses. He established the Lahore University of Management Sciences (LUMS) in 1985. He also served as Pakistan’s Minister of Finance, Economic Affairs & Planning in 1993.</p>
                    
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->

    <!-- Division Box -->
    <section class="division-box graduate-box padtb-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/convo18/1.png" alt="Mohammad Zubair" class="division-mage">
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Honorable Governor, Sindh</h3>
                     <h5>Mohammad Zubair</h5>
                     <p>Mohammad Zubair was appointed as the 32nd Governor of Sindh and took his oath of office in February 2017. After completing his master's degree, he joined IBM from 1981 until his resignation in 2007. Umar was Chairman of the Pakistan Board of Investment from July to 2013 to 17 December 2013. He also served as Chairman of Privatization Commission of Pakistan from December 2013 until February 2017.</p>
                    
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->
   
      <!-- Division Box -->   
      <section class="division-box graduate-box padb-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/convo18/3.png" alt="Saadia Asad Pathan" class="division-mage">
                     <a  href="https://youtu.be/idrAYeRfZ9Q" data-fancybox="gallery" class="play-icon-box">
                        <img src="img/playicon.svg" alt="">
                     </a>
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Valedictorian</h3>
                     <h5>Saadia Asad Pathan</h5>
                     <h6>Valedictorian, Class of 2018</h6>
                     <p>“We call ourselves Pakistan’s first liberal arts university but we’re not just the first. We’re also new and being new is far scarier than being the first. But I believe that it’s only the adventure of being new that has given us all, students, faculty, and staff, the freedom to be individuals. To push boundaries, to experiment beyond the traditions and expectations that define institutions much older than us both here and abroad."</p>
                     
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->
   
      <!-- Division Box -->   
      <section class="division-box graduate-box padb-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/convo18/4.png" alt="Awards Night" class="division-mage">
                     <a  href="https://youtu.be/-eatYcM83Yg" data-fancybox="gallery"  class="play-icon-box">
                        <img src="img/playicon.svg" alt="">
                     </a>
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Awards Night</h3>
                     <p>In recognition of the high academic achievements of the Class of 2018, Habib University held an awards night ceremony on campus. The event was one in which the first graduating cohort, the Trailblazers, were recognized for their achievements. The event also recognized the University’s founding faculty members</p>
                   
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->
   
      <section class="graduate-area">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Graduate Directory</h3>
                     <p>The graduate directory serve as depository of information for recruitment by potential employers.</p>
                     <a target="_blank" href="https://habib.edu.pk/career-services/request-for-graduate-directory/" target="_blank" class="cooming-soon-btn">
                        <div class="btn-hover-down">
                           <span class="pdf-coming"><i class="far fa-file-pdf"></i>  Download Now</span>
                           <span class="pdf-download"><i class="fas fa-download"></i>  Download Now</span>
                        </div>
                     </a>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="grdaute-heading">
                   <img src="img/graduate-text.svg" alt="">
                  </div>
               </div>
            </div>
         </div>
      </section>
   </div>  


   

   

<?php include 'include/footer.php' ?>